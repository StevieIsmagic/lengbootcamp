//
//  ContentView.swift
//  lengBootcamp
//
//  Created by Steven Ocampo on 3/20/20.
//  Copyright © 2020 StevenOcampo. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
